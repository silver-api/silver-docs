﻿Configuration
=========
```json
{
  "clientInfo": {
    "verMajor": 8,
    "verMiddle": 332,
    "verMinor": 10,
    "masterhash": "b288f38c54fafee41b5331e2a60e46ec809f95c0",
    "publicKey": "bb9ca4c6b52ecdb40267c3bcca03679201a403ef6230b9e488db949b58bc7479",
    "keyVersion": 9
  },
  "connection": {
    "timeout": 5000,
    "port": 9339,
    "url": "game.clashofclans.com",
    "limit": 1
  },
  "server": {
    "ip": "*",
    "port": 80,
    "basePath": "/api/",
    "keepAlive": false,
    "useDeflate": true
  },
  "users": [
    {
      "displayName": "Client 1",
      "userId": 0,
      "userToken": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
      "exposedName": "c1",
      "disabled": false
    }
  ],
  "callbacks": [
    {
      "url": "http://127.0.0.1:8080/csCallbackNotify.php",
      "subscriptions": "warInfo warStars warHistory clanInfo",
      "clanId": 0
    }
  ],
  "advanced": {
    "forwarders": {
      "player_village": "userVillage"
    },
    "disable": "shutdown cachedbg",
    "password": "%pass%",
    "offlineCaching": true,
    "naturalizeZeroIndexedLevels": true,
    "outputEnumNames": true
  },
  "auth": {
    "name": "NAME_HERE",
    "key": "KEY_HERE"
  }
}
```
*The default configuration file: `config.json`, generated if an existing config file does not exist.*

# Basic Setup
The basic setup required to get Silver connecting and serving API requests is rather simple:
 - Add 1 or more proper user objects to the `users[]` array. Only a valid `userId` and `userToken` are required. If you need help obtaining IDs/tokens, see our [Skype group](https://join.skype.com/BZCFlDbztzKg).
   - Update the `connection.limit` parameter if you wish for multiple simultaneous clients to remain active at once. It's generally suggested to leave this value less than the count of `users[]`, so there will always be an available client to load-in if others enter personal break.
 - Fill out the `auth` section with your name and key, as provided upon download.
 - *Optional:* update the `clientInfo` section with the current encryption key, if connection fails only. You will normally be provided with the updated information, and it shall be reflected also on this page.

Once one has completed the above, Silver should properly connect using provided clients.

&nbsp;

The web-API should be accessible using your local-IP (unless `server.ip` was changed), on port `server.port`. Commands should be preceded by the path `server.basePath` in the URL.

As an example using the default config, one would execute the `info` command like so:
```
http://127.0.0.1:80/api/info
```
Note that the `:80` is assumed by default if no port is specified. Thus, one must only truly specify the `:port` if `server.port` was changed to a value other than 80.

# `clientInfo` object
This object defines the basic information needed to establish any connection with the Clash of Clans server. Namely, the version and encryption information.
```json
"clientInfo": {
  "verMajor": 8,
  "verMiddle": 332,
  "verMinor": 10,
  "masterhash": "b288f38c54fafee41b5331e2a60e46ec809f95c0",
  "publicKey": "bb9ca4c6b52ecdb40267c3bcca03679201a403ef6230b9e488db949b58bc7479",
  "keyVersion": 9
}
```

| **Name**     | **Description**                                 | **Notes**                            |
|--------------|-------------------------------------------------|--------------------------------------|
| `verMajor`   | The current major-version of the client.        | The '8' in 8.332.10.                 |
| `verMiddle`  | The current middle-version of the client.       | The '332' in 8.332.10.               |
| `verMinor`   | The current minor-version of the client.        | The '10' in 8.332.10.                |
| `masterhash` | A hash representing the current version's data. | Found in `fingerprint.json`.         |
| `publicKey`  | The public-key used for packet encryption.      | Embedded in client  binary.          |
| `keyVersion` | The version of the encryption system to use.    | A simple count, based on PK changes. |
There is generally some room for the version information (including `masterhash`) to be inaccurate. As long as the provided version existed at some point in the past, a connection can typically be made to 'upgrade' this info. Silver will output warnings when such an event occurs.

# `connection` object
This object defines rules that clients only are restricted to follow. To configure connection preferences for the API web-server, see the separate `server` object.
```json
"connection": {
  "timeout": 5000,
  "port": 9339,
  "url": "game.clashofclans.com",
  "limit": 1
}
```

| **Name**  | **Description**                                                                                         | **Notes**                                                                                                               |
|-----------|---------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| `timeout` | The number of milliseconds a client should spend waiting for a response from the Clash of Clans server. | After expiring, any dependent API request will instead send a timeout notification.                                     |
| `port`    | The port which should be used in connecting to the Clash of Clans server.                               | Should be left as 9339.                                                                                                 |
| `url`     | The URL of the Clash of Clans game-server.                                                              | Should be left as 'game.clashofclans.com'.                                                                              |
| `limit`   | The maximum count of clients which can be connected at the same time.                                   | It's generally a good idea to leave this one or two less than the number of `users[]`, to ensure always-active service. |

For most general-purpose usage, the default settings for this group should suffice, though some users may want to adjust the `limit` value, to enable multiple concurrent connections.

Further, while Silver can function with the Android variant of the `url` value ('gamea.' rather than 'game.'), using it would be a *very* poor choice, as Silver will always imitate an iOS device internally. Bans may result from using a non-matching `url`, given Supercell's strengthened initiative towards preventing third-party software.

# `server` object
The `server` object defines how Silver's web-server API should work, and is generally satisfactory being left at its default.
```json
"server": {
  "ip": "*",
  "port": 80,
  "basePath": "/api/",
  "keepAlive": false,
  "useDeflate": true
}
```

| **Name**     | **Description**                                                                                                              | **Notes**                                                                                                                                                                                                                  |
|--------------|------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ip`         | The local-IP to host the server on.                                                                                          | It's generally best to use the wildcard-target `*`, to ensure Silver is accessible from a public-IP, if needed.                                                                                                            |
| `port`       | The port which the server should target.                                                                                     | Usage of the HTTP default (80) is highly recommended.                                                                                                                                                                      |
| `basePath`   | The base-URL which commands should be accessed from.                                                                         | A value of `"/"` or `""` can be set to essentially remove any path requirement.                                                                                                                                            |
| `keepAlive`  | Determines whether or not the `keep-alive` value should be honored within the connection header.                             | This value is best left as `false`, due to issues with older PHP versions in making requests otherwise.                                                                                                                    |
| `useDeflate` | Determines whether or not deflate compression should be used for the returned data, which can help reduce transmission time. | If using `file_get_contents` from older versions of PHP while `useDeflate` is active, the output will appear as a useless bunch of characters, as the response is not decompressed. If this is an issue, use cURL instead. |

As described already in the [Commands](../command/commands.md) documentation, the general format for requests is as follows:

`http://` + `server.ip` + `:server.port` + `server.basePath` + `commandName` + `?param1=val1` + `&param2=val2` + `...`

As an example using the default config, and the [`clanSearch`](../command/clanSearch.md) command:
```
http://127.0.0.1/api/clanSearch?name=Ha&region=ITALY&warFrequency=ALWAYS
```

# `users[]` list
This array is composed of `user` objects, which simply define the profiles that spawned clients should use. At least one `user` is required in order for Silver to function.
```json
"users": [
  {
    "displayName": "Client 1",
    "userId": 0,
    "userToken": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "exposedName": "c1",
    "disabled": false
  },
  {
    ANOTHER_USER_OBJECT_HERE....
  }
]
```

| **Name**      | **Description**                                                                                                                              | **Notes**                                                                                                   |
|---------------|----------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| `displayName` | A human-readable display name used to describe the client.                                                                                   | Optional, but recommended.                                                                                  |
| `userId`      | The user-ID associated with the profile.                                                                                                     | Required. Must be a long-integer value.                                                                     |
| `userToken`   | The user-token associated with the given `userId`. Essentially a password.                                                                   | Required. Must be a string value.                                                                           |
| `exposedName` | The string identifier associated with any client spawned using the profile.                                                                  | Optional. Can be used to force a certain profile to execute a command, rather than the first available one. |
| `disabled`    | A boolean value describing whether or not the user information should be used. If set to `true`, the user will be ignored when config loads. | Optional. Default value of `false`.                                                                         |

This section is the most important to get correct: any mismatching `userId` or `userToken` will remove the user from the client-pool, preventing it from being used.

As it is difficult to retrieve `userToken` values without a proxy setup, it is possible to receive some assistance in doing so, via our [Skype group](https://join.skype.com/BZCFlDbztzKg). We can retrieve this information using a partial-link (*Settings -> Link a Device*), which is not permanent, and does not compromise account data. For the overly cautious, a new account may be setup and provided instead.

&nbsp;

*Generally, it's a good idea to use dummy/mule accounts for Silver, as there's always a risk of being banned when using third-party software. This risk, however, is estimated to be very low.*


# `callbacks[]` list
This array is composed of `callback` objects, describing which events should be monitored for, the target clans, and the callback-responders.
```json
"callbacks": [
  {
    "url": "http://127.0.0.1:8080/csCallbackNotify.php",
    "subscriptions": "warInfo warStars warHistory clanInfo",
    "clanId": 0
  },
  {
    ANOTHER_CALLBACK_OBJECT_HERE....
  }
]
```

| **Name**        | **Description**                                                      | **Notes**                                                        |
|-----------------|----------------------------------------------------------------------|------------------------------------------------------------------|
| `url`           | URL of the responder meant to handle events for this `callback`.     | See the [Callback](../callback/callbacks.md) documentation page. |
| `subscriptions` | The events which should be handled for the target `clanId`.          | See the [Callback](../callback/callbacks.md) documentation page. |
| `clanId`        | Identifier for the clan which should be targeted by this `callback`. | See the [Callback](../callback/callbacks.md) documentation page. |

While the config keys are fairly self-explanatory, their usage is not so much. Reading the [Callback](../callback/callbacks.md) page is recommended for those interested in setting up realtime-monitoring for their clan.

# `advanced` object
Don't touch this.

# `auth` object
This object serves to validate your license to use Silver, and without it, the program will end shortly after the client launch phase completes.
```json
"auth": {
  "name": "NAME_HERE",
  "key": "KEY_HERE"
}
```

| **Name** | **Description**                               | **Notes**         |
|----------|-----------------------------------------------|-------------------|
| `name`   | The name associated with your Silver license. | Case-sensitive.   |
| `key`    | The key associated with your Silver license.  | Case-insensitive. |

Silver licenses are non-transferable, so it'd be great if you could keep your key private. Additionally, any key made public will be permanently banned, with a replacement being issued at our discretion.
