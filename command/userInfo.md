﻿`userInfo` Command
=========
Returns profile-information for the specified player.

# Parameters
| **Name**   | **Type** | **Description**         | **Notes**                                                   |
|------------|----------|-------------------------|-------------------------------------------------------------|
| `id`       | `id`     | The player's user-ID.   |                                                             |
| *`homeId`* | *`id`*   | *The player's home-ID.* | *Optional. If not provided, `id` will be assumed for both.* |

# Example
```
http://127.0.0.1/api/userInfo?id=73014585410
```
```json
{
  "userInfo": {
    "userId": 73014585410,
    "homeId": 73014585410,
    "clan": {
      "clanId": 103080058097,
      "clanName": "The Renegades",
      "clanBadge": 1778392666,
      "clanRole": "ELDER",
      "clanLevel": 9
    },
    "leagueId": 137439384916,
    "legendLeagueStats": {
      "legendTrophies": 0,
      "bestSeasonId": 0,
      "bestSeasonMonth": 0,
      "bestSeasonYear": 0,
      "bestSeasonRank": 0,
      "bestSeasonTrophies": 0,
      "previousSeasonId": 0,
      "previousSeasonMonth": 0,
      "previousSeasonYear": 0,
      "previousSeasonRank": 0,
      "previousSeasonTrophies": 0
    },
    "leagueType": "CHAMPION_II",
    "clanCastleLevel": 5,
    "clanCastleCapacity": 30,
    "clanCastleUsedSpace": 0,
    "townHallLevel": 10,
    "userName": "wiiness",
    "facebookId": "",
    "level": 129,
    "exp": 6285,
    "gemCount": -350163649,
    "trophyCount": 3446,
    "attacksWon": 150,
    "attacksLost": -936113666,
    "defensesWon": 15,
    "defensesLost": -568349565,
    "nameChosenByUser": true,
    "nameChangeCount": 0,
    "boughtGems": -489408979,
    "resourceCaps": [
      {
        "type": "GOLD",
        "value": 8500000
      },
      {
        "type": "ELIXIR",
        "value": 8500000
      },
      {
        "type": "DARK_ELIXIR",
        "value": 200000
      },
      {
        "type": "TREASURY_GOLD",
        "value": 2800000
      },
      {
        "type": "TREASURY_ELIXIR",
        "value": 2800000
      },
      {
        "type": "TREASURY_DARK_ELIXIR",
        "value": 14000
      }
    ],
    "resourceValues": [
      {
        "type": "GOLD",
        "value": 3811611
      },
      {
        "type": "ELIXIR",
        "value": 3275319
      },
      {
        "type": "DARK_ELIXIR",
        "value": 19170
      },
      {
        "type": "TREASURY_GOLD",
        "value": 0
      },
      {
        "type": "TREASURY_ELIXIR",
        "value": 0
      },
      {
        "type": "TREASURY_DARK_ELIXIR",
        "value": 0
      }
    ],
    "armyTroops": [
      {
        "type": "BARBARIAN",
        "count": 0
      },
      {
        "type": "ARCHER",
        "count": 0
      },
      {
        "type": "GOBLIN",
        "count": 0
      },
      {
        "type": "GIANT",
        "count": 12
      },
      {
        "type": "WALL_BREAKER",
        "count": 6
      },
      {
        "type": "BALLOON",
        "count": 0
      },
      {
        "type": "WIZARD",
        "count": 14
      },
      {
        "type": "HEALER",
        "count": 0
      },
      {
        "type": "DRAGON",
        "count": 0
      },
      {
        "type": "PEKKA",
        "count": 0
      },
      {
        "type": "MINION",
        "count": 0
      },
      {
        "type": "HOG_RIDER",
        "count": 0
      },
      {
        "type": "VALKYRIE",
        "count": 10
      },
      {
        "type": "GOLEM",
        "count": 0
      },
      {
        "type": "WITCH",
        "count": 0
      },
      {
        "type": "LAVA_HOUND",
        "count": 0
      },
      {
        "type": "BABY_DRAGON",
        "count": 0
      }
    ],
    "armySpells": [
      {
        "type": "LIGHTNING_SPELL",
        "count": 0
      },
      {
        "type": "HEALING_SPELL",
        "count": 0
      },
      {
        "type": "RAGE_SPELL",
        "count": 2
      },
      {
        "type": "JUMP_SPELL",
        "count": 1
      },
      {
        "type": "FREEZE_SPELL",
        "count": 0
      },
      {
        "type": "POISON_SPELL",
        "count": 1
      },
      {
        "type": "EARTHQUAKE_SPELL",
        "count": 0
      },
      {
        "type": "HASTE_SPELL",
        "count": 0
      },
      {
        "type": "CLONE_SPELL",
        "count": 0
      },
      {
        "type": "SKELETON_SPELL",
        "count": 0
      }
    ],
    "troopLevels": [
      {
        "type": "BARBARIAN",
        "level": 7
      },
      {
        "type": "ARCHER",
        "level": 7
      },
      {
        "type": "GOBLIN",
        "level": 6
      },
      {
        "type": "GIANT",
        "level": 7
      },
      {
        "type": "WALL_BREAKER",
        "level": 6
      },
      {
        "type": "BALLOON",
        "level": 6
      },
      {
        "type": "WIZARD",
        "level": 6
      },
      {
        "type": "HEALER",
        "level": 4
      },
      {
        "type": "DRAGON",
        "level": 3
      },
      {
        "type": "PEKKA",
        "level": 4
      },
      {
        "type": "MINION",
        "level": 6
      },
      {
        "type": "HOG_RIDER",
        "level": 6
      },
      {
        "type": "VALKYRIE",
        "level": 5
      },
      {
        "type": "GOLEM",
        "level": 5
      },
      {
        "type": "WITCH",
        "level": 2
      },
      {
        "type": "LAVA_HOUND",
        "level": 3
      }
    ],
    "spellLevels": [
      {
        "type": "LIGHTNING_SPELL",
        "level": 5
      },
      {
        "type": "HEALING_SPELL",
        "level": 6
      },
      {
        "type": "RAGE_SPELL",
        "level": 5
      },
      {
        "type": "JUMP_SPELL",
        "level": 2
      },
      {
        "type": "FREEZE_SPELL",
        "level": 5
      },
      {
        "type": "POISON_SPELL",
        "level": 4
      },
      {
        "type": "EARTHQUAKE_SPELL",
        "level": 4
      },
      {
        "type": "HASTE_SPELL",
        "level": 4
      }
    ],
    "heroLevels": [
      {
        "hero": "BARBARIAN_KING",
        "level": 21
      },
      {
        "hero": "ARCHER_QUEEN",
        "level": 26
      }
    ],
    "heroHPValues": [
      {
        "hero": "BARBARIAN_KING",
        "hp": 26
      },
      {
        "hero": "ARCHER_QUEEN",
        "hp": 326
      }
    ],
    "heroStates": [
      {
        "hero": "BARBARIAN_KING",
        "state": "GUARDING"
      },
      {
        "hero": "ARCHER_QUEEN",
        "state": "GUARDING"
      }
    ],
    "castleEntries": [
      {
        "type": "BARBARIAN",
        "count": 0,
        "level": 1
      },
      {
        "type": "BARBARIAN",
        "count": 0,
        "level": 2
      },
      {
        "type": "BARBARIAN",
        "count": 0,
        "level": 3
      },
      {
        "type": "BARBARIAN",
        "count": 0,
        "level": 4
      },
      {
        "type": "BARBARIAN",
        "count": 0,
        "level": 5
      },
      {
        "type": "BARBARIAN",
        "count": 0,
        "level": 6
      },
      {
        "type": "ARCHER",
        "count": 0,
        "level": 2
      },
      {
        "type": "ARCHER",
        "count": 0,
        "level": 3
      },
      {
        "type": "ARCHER",
        "count": 0,
        "level": 4
      },
      {
        "type": "ARCHER",
        "count": 0,
        "level": 5
      },
      {
        "type": "ARCHER",
        "count": 0,
        "level": 6
      },
      {
        "type": "ARCHER",
        "count": 0,
        "level": 7
      },
      {
        "type": "GOBLIN",
        "count": 0,
        "level": 3
      },
      {
        "type": "GOBLIN",
        "count": 0,
        "level": 5
      },
      {
        "type": "GIANT",
        "count": 0,
        "level": 1
      },
      {
        "type": "GIANT",
        "count": 0,
        "level": 2
      },
      {
        "type": "GIANT",
        "count": 0,
        "level": 3
      },
      {
        "type": "GIANT",
        "count": 0,
        "level": 4
      },
      {
        "type": "GIANT",
        "count": 0,
        "level": 5
      },
      {
        "type": "GIANT",
        "count": 0,
        "level": 6
      },
      {
        "type": "GIANT",
        "count": 0,
        "level": 7
      },
      {
        "type": "WALL_BREAKER",
        "count": 0,
        "level": 3
      },
      {
        "type": "WALL_BREAKER",
        "count": 0,
        "level": 5
      },
      {
        "type": "BALLOON",
        "count": 0,
        "level": 2
      },
      {
        "type": "BALLOON",
        "count": 0,
        "level": 5
      },
      {
        "type": "BALLOON",
        "count": 0,
        "level": 6
      },
      {
        "type": "BALLOON",
        "count": 0,
        "level": 7
      },
      {
        "type": "WIZARD",
        "count": 0,
        "level": 2
      },
      {
        "type": "WIZARD",
        "count": 0,
        "level": 4
      },
      {
        "type": "WIZARD",
        "count": 0,
        "level": 5
      },
      {
        "type": "WIZARD",
        "count": 0,
        "level": 6
      },
      {
        "type": "HEALER",
        "count": 0,
        "level": 1
      },
      {
        "type": "HEALER",
        "count": 0,
        "level": 2
      },
      {
        "type": "DRAGON",
        "count": 0,
        "level": 1
      },
      {
        "type": "DRAGON",
        "count": 0,
        "level": 2
      },
      {
        "type": "DRAGON",
        "count": 0,
        "level": 3
      },
      {
        "type": "DRAGON",
        "count": 0,
        "level": 5
      },
      {
        "type": "PEKKA",
        "count": 0,
        "level": 3
      },
      {
        "type": "PEKKA",
        "count": 0,
        "level": 4
      },
      {
        "type": "PEKKA",
        "count": 0,
        "level": 5
      },
      {
        "type": "MINION",
        "count": 0,
        "level": 6
      },
      {
        "type": "HOG_RIDER",
        "count": 0,
        "level": 1
      },
      {
        "type": "HOG_RIDER",
        "count": 0,
        "level": 2
      },
      {
        "type": "HOG_RIDER",
        "count": 0,
        "level": 3
      },
      {
        "type": "HOG_RIDER",
        "count": 0,
        "level": 4
      },
      {
        "type": "HOG_RIDER",
        "count": 0,
        "level": 5
      },
      {
        "type": "GOLEM",
        "count": 0,
        "level": 1
      },
      {
        "type": "GOLEM",
        "count": 0,
        "level": 3
      },
      {
        "type": "GOLEM",
        "count": 0,
        "level": 4
      },
      {
        "type": "GOLEM",
        "count": 0,
        "level": 5
      },
      {
        "type": "WITCH",
        "count": 0,
        "level": 2
      },
      {
        "type": "LAVA_HOUND",
        "count": 0,
        "level": 2
      },
      {
        "type": "POISON_SPELL",
        "count": 0,
        "level": 3
      },
      {
        "type": "POISON_SPELL",
        "count": 0,
        "level": 4
      },
      {
        "type": "EARTHQUAKE_SPELL",
        "count": 0,
        "level": 3
      },
      {
        "type": "HASTE_SPELL",
        "count": 0,
        "level": 2
      },
      {
        "type": "HASTE_SPELL",
        "count": 0,
        "level": 3
      },
      {
        "type": "SKELETON_SPELL",
        "count": 0,
        "level": 2
      }
    ],
    "tutorialQuestsCompleted": [
      21000000,
      21000001,
      21000002,
      21000003,
      21000004,
      21000005,
      21000006,
      21000007,
      21000008,
      21000009,
      21000010,
      21000011,
      21000012,
      21000013,
      21000014,
      21000015
    ],
    "achievementsCompleted": [
      "BIGGER_COFFERS_1",
      "BIGGER_COFFERS_2",
      "BIGGER_COFFERS_3",
      "GET_THOSE_GOBLINS_1",
      "GET_THOSE_GOBLINS_2",
      "GET_THOSE_GOBLINS_3",
      "BIGGER_AND_BETTER_1",
      "BIGGER_AND_BETTER_2",
      "BIGGER_AND_BETTER_3",
      "NICE_AND_TIDY_1",
      "NICE_AND_TIDY_2",
      "NICE_AND_TIDY_3",
      "RELEASE_THE_BEASTS_1",
      "RELEASE_THE_BEASTS_2",
      "RELEASE_THE_BEASTS_3",
      "GOLD_GRAB_1",
      "GOLD_GRAB_2",
      "GOLD_GRAB_3",
      "ELIXIR_ESCAPADE_1",
      "ELIXIR_ESCAPADE_2",
      "ELIXIR_ESCAPADE_3",
      "SWEET_VICTORY_1",
      "SWEET_VICTORY_2",
      "SWEET_VICTORY_3",
      "EMPIRE_BUILDER_1",
      "EMPIRE_BUILDER_2",
      "EMPIRE_BUILDER_3",
      "WALL_BUSTER_1",
      "WALL_BUSTER_2",
      "WALL_BUSTER_3",
      "HUMILIATOR_1",
      "HUMILIATOR_2",
      "HUMILIATOR_3",
      "UNION_BUSTER_1",
      "UNION_BUSTER_2",
      "UNION_BUSTER_3",
      "CONQUEROR_1",
      "CONQUEROR_2",
      "CONQUEROR_3",
      "UNBREAKABLE_1",
      "UNBREAKABLE_2",
      "FRIEND_IN_NEED_1",
      "FRIEND_IN_NEED_2",
      "FRIEND_IN_NEED_3",
      "MORTAR_MAULER_1",
      "MORTAR_MAULER_2",
      "MORTAR_MAULER_3",
      "HEROIC_HEIST_1",
      "HEROIC_HEIST_2",
      "HEROIC_HEIST_3",
      "LEAGUE_ALL_STAR_1",
      "LEAGUE_ALL_STAR_2",
      "LEAGUE_ALL_STAR_3",
      "X_BOW_EXTERMINATOR_1",
      "X_BOW_EXTERMINATOR_2",
      "FIREFIGHTER_1",
      "FIREFIGHTER_2",
      "WAR_HERO_1",
      "TREASURER_1",
      "TREASURER_2",
      "SHARING_IS_CARING_1"
    ],
    "achievementProgress": [
      {
        "achievement": "BIGGER_COFFERS_1",
        "value": 11
      },
      {
        "achievement": "BIGGER_COFFERS_2",
        "value": 11
      },
      {
        "achievement": "BIGGER_COFFERS_3",
        "value": 11
      },
      {
        "achievement": "GET_THOSE_GOBLINS_1",
        "value": 150
      },
      {
        "achievement": "GET_THOSE_GOBLINS_2",
        "value": 150
      },
      {
        "achievement": "GET_THOSE_GOBLINS_3",
        "value": 150
      },
      {
        "achievement": "BIGGER_AND_BETTER_1",
        "value": 10
      },
      {
        "achievement": "BIGGER_AND_BETTER_2",
        "value": 10
      },
      {
        "achievement": "BIGGER_AND_BETTER_3",
        "value": 10
      },
      {
        "achievement": "NICE_AND_TIDY_1",
        "value": 2582
      },
      {
        "achievement": "NICE_AND_TIDY_2",
        "value": 2582
      },
      {
        "achievement": "NICE_AND_TIDY_3",
        "value": 2582
      },
      {
        "achievement": "RELEASE_THE_BEASTS_1",
        "value": 1
      },
      {
        "achievement": "RELEASE_THE_BEASTS_2",
        "value": 1
      },
      {
        "achievement": "RELEASE_THE_BEASTS_3",
        "value": 1
      },
      {
        "achievement": "GOLD_GRAB_1",
        "value": 569493082
      },
      {
        "achievement": "GOLD_GRAB_2",
        "value": 569493082
      },
      {
        "achievement": "GOLD_GRAB_3",
        "value": 569493082
      },
      {
        "achievement": "ELIXIR_ESCAPADE_1",
        "value": 663726439
      },
      {
        "achievement": "ELIXIR_ESCAPADE_2",
        "value": 663726439
      },
      {
        "achievement": "ELIXIR_ESCAPADE_3",
        "value": 663726439
      },
      {
        "achievement": "SWEET_VICTORY_1",
        "value": 3540
      },
      {
        "achievement": "SWEET_VICTORY_2",
        "value": 3540
      },
      {
        "achievement": "SWEET_VICTORY_3",
        "value": 3540
      },
      {
        "achievement": "EMPIRE_BUILDER_1",
        "value": 5
      },
      {
        "achievement": "EMPIRE_BUILDER_2",
        "value": 5
      },
      {
        "achievement": "EMPIRE_BUILDER_3",
        "value": 5
      },
      {
        "achievement": "WALL_BUSTER_1",
        "value": 87331
      },
      {
        "achievement": "WALL_BUSTER_2",
        "value": 87331
      },
      {
        "achievement": "WALL_BUSTER_3",
        "value": 87331
      },
      {
        "achievement": "HUMILIATOR_1",
        "value": 4373
      },
      {
        "achievement": "HUMILIATOR_2",
        "value": 4373
      },
      {
        "achievement": "HUMILIATOR_3",
        "value": 4373
      },
      {
        "achievement": "UNION_BUSTER_1",
        "value": 13078
      },
      {
        "achievement": "UNION_BUSTER_2",
        "value": 13078
      },
      {
        "achievement": "UNION_BUSTER_3",
        "value": 13078
      },
      {
        "achievement": "CONQUEROR_1",
        "value": 5768
      },
      {
        "achievement": "CONQUEROR_2",
        "value": 5768
      },
      {
        "achievement": "CONQUEROR_3",
        "value": 5768
      },
      {
        "achievement": "UNBREAKABLE_1",
        "value": 695
      },
      {
        "achievement": "UNBREAKABLE_2",
        "value": 695
      },
      {
        "achievement": "UNBREAKABLE_3",
        "value": 695
      },
      {
        "achievement": "FRIEND_IN_NEED_1",
        "value": 37872
      },
      {
        "achievement": "FRIEND_IN_NEED_2",
        "value": 37872
      },
      {
        "achievement": "FRIEND_IN_NEED_3",
        "value": 37872
      },
      {
        "achievement": "MORTAR_MAULER_1",
        "value": 11724
      },
      {
        "achievement": "MORTAR_MAULER_2",
        "value": 11724
      },
      {
        "achievement": "MORTAR_MAULER_3",
        "value": 11724
      },
      {
        "achievement": "HEROIC_HEIST_1",
        "value": 2242839
      },
      {
        "achievement": "HEROIC_HEIST_2",
        "value": 2242839
      },
      {
        "achievement": "HEROIC_HEIST_3",
        "value": 2242839
      },
      {
        "achievement": "LEAGUE_ALL_STAR_1",
        "value": 17
      },
      {
        "achievement": "LEAGUE_ALL_STAR_2",
        "value": 17
      },
      {
        "achievement": "LEAGUE_ALL_STAR_3",
        "value": 17
      },
      {
        "achievement": "X_BOW_EXTERMINATOR_1",
        "value": 1964
      },
      {
        "achievement": "X_BOW_EXTERMINATOR_2",
        "value": 1964
      },
      {
        "achievement": "X_BOW_EXTERMINATOR_3",
        "value": 1964
      },
      {
        "achievement": "FIREFIGHTER_1",
        "value": 451
      },
      {
        "achievement": "FIREFIGHTER_2",
        "value": 451
      },
      {
        "achievement": "FIREFIGHTER_3",
        "value": 451
      },
      {
        "achievement": "WAR_HERO_1",
        "value": 135
      },
      {
        "achievement": "WAR_HERO_2",
        "value": 135
      },
      {
        "achievement": "WAR_HERO_3",
        "value": 135
      },
      {
        "achievement": "TREASURER_1",
        "value": 57677709
      },
      {
        "achievement": "TREASURER_2",
        "value": 57677709
      },
      {
        "achievement": "TREASURER_3",
        "value": 57677709
      },
      {
        "achievement": "SHARING_IS_CARING_1",
        "value": 262
      },
      {
        "achievement": "SHARING_IS_CARING_2",
        "value": 262
      },
      {
        "achievement": "SHARING_IS_CARING_3",
        "value": 262
      }
    ],
    "campaignScores": [
      {
        "campaignStage": "PAYBACK",
        "stars": 3
      },
      {
        "campaignStage": "GOBLIN_FOREST",
        "stars": 3
      },
      {
        "campaignStage": "GOBLIN_OUTPOST",
        "stars": 3
      },
      {
        "campaignStage": "ROCKY_FORT",
        "stars": 3
      },
      {
        "campaignStage": "GOBLIN_GAUNTLET",
        "stars": 3
      },
      {
        "campaignStage": "CANNONBALL_RUN",
        "stars": 3
      },
      {
        "campaignStage": "TWO_SMOKING_BARRELS",
        "stars": 3
      },
      {
        "campaignStage": "GOLD_RUSH",
        "stars": 3
      },
      {
        "campaignStage": "MAGINOT_LINE",
        "stars": 3
      },
      {
        "campaignStage": "RAT_VALLEY",
        "stars": 3
      },
      {
        "campaignStage": "BRUTE_FORCE",
        "stars": 3
      },
      {
        "campaignStage": "GOBBOTOWN",
        "stars": 3
      },
      {
        "campaignStage": "M_IS_FOR_MORTAR",
        "stars": 3
      },
      {
        "campaignStage": "MEGABLASTER",
        "stars": 3
      },
      {
        "campaignStage": "IMMOVABLE_OBJECT",
        "stars": 3
      },
      {
        "campaignStage": "FORT_KNOBS",
        "stars": 3
      },
      {
        "campaignStage": "WATCHTOWER",
        "stars": 3
      },
      {
        "campaignStage": "FOOLS_GOLD",
        "stars": 3
      },
      {
        "campaignStage": "THOROUGHFARE",
        "stars": 3
      },
      {
        "campaignStage": "BOUNCY_CASTLE",
        "stars": 3
      },
      {
        "campaignStage": "FIKOVA",
        "stars": 3
      },
      {
        "campaignStage": "GOBBO_CAMPUS",
        "stars": 3
      },
      {
        "campaignStage": "DANNY_BOY",
        "stars": 3
      },
      {
        "campaignStage": "OMMAHHA_BEECH",
        "stars": 3
      },
      {
        "campaignStage": "WALLS_OF_STEEL",
        "stars": 3
      },
      {
        "campaignStage": "SICILIAN_DEFENSE",
        "stars": 3
      },
      {
        "campaignStage": "OBSIDIAN_TOWER",
        "stars": 3
      },
      {
        "campaignStage": "ARROW_HEAD",
        "stars": 3
      },
      {
        "campaignStage": "RED_CARPET",
        "stars": 3
      },
      {
        "campaignStage": "NATURAL_DEFENSE",
        "stars": 3
      },
      {
        "campaignStage": "STEEL_GAUNTLET",
        "stars": 3
      },
      {
        "campaignStage": "QUEENS_GAMBIT",
        "stars": 3
      },
      {
        "campaignStage": "FULL_FRONTAL",
        "stars": 3
      },
      {
        "campaignStage": "CHIMP_IN_ARMOR",
        "stars": 3
      },
      {
        "campaignStage": "FAULTY_TOWERS",
        "stars": 3
      },
      {
        "campaignStage": "POINT_MAN",
        "stars": 3
      },
      {
        "campaignStage": "TRIPLE_A",
        "stars": 3
      },
      {
        "campaignStage": "GOBLIN_PICNIC",
        "stars": 3
      },
      {
        "campaignStage": "BAIT_N_SWITCH",
        "stars": 3
      },
      {
        "campaignStage": "COLLATERAL_DAMAGE",
        "stars": 3
      },
      {
        "campaignStage": "CHOOSE_WISELY",
        "stars": 3
      },
      {
        "campaignStage": "MEGA_EVIL",
        "stars": 3
      },
      {
        "campaignStage": "CRYSTAL_CRUST",
        "stars": 3
      },
      {
        "campaignStage": "COLD_AS_ICE",
        "stars": 3
      },
      {
        "campaignStage": "JUMP_AROUND",
        "stars": 3
      },
      {
        "campaignStage": "KITCHEN_SINK",
        "stars": 3
      },
      {
        "campaignStage": "ROLLING_TERROR",
        "stars": 3
      },
      {
        "campaignStage": "MEGAMANSION",
        "stars": 3
      },
      {
        "campaignStage": "PEKKAS_PLAYHOUSE",
        "stars": 3
      },
      {
        "campaignStage": "SHERBET_TOWERS",
        "stars": 3
      }
    ],
    "campaignGoldLoot": [
      {
        "campaignStage": "PAYBACK",
        "loot": 500
      },
      {
        "campaignStage": "GOBLIN_FOREST",
        "loot": 500
      },
      {
        "campaignStage": "GOBLIN_OUTPOST",
        "loot": 500
      },
      {
        "campaignStage": "ROCKY_FORT",
        "loot": 1000
      },
      {
        "campaignStage": "GOBLIN_GAUNTLET",
        "loot": 1000
      },
      {
        "campaignStage": "CANNONBALL_RUN",
        "loot": 1000
      },
      {
        "campaignStage": "TWO_SMOKING_BARRELS",
        "loot": 2000
      },
      {
        "campaignStage": "GOLD_RUSH",
        "loot": 2000
      },
      {
        "campaignStage": "MAGINOT_LINE",
        "loot": 2000
      },
      {
        "campaignStage": "RAT_VALLEY",
        "loot": 3000
      },
      {
        "campaignStage": "BRUTE_FORCE",
        "loot": 3000
      },
      {
        "campaignStage": "GOBBOTOWN",
        "loot": 3000
      },
      {
        "campaignStage": "M_IS_FOR_MORTAR",
        "loot": 4000
      },
      {
        "campaignStage": "MEGABLASTER",
        "loot": 4000
      },
      {
        "campaignStage": "IMMOVABLE_OBJECT",
        "loot": 4000
      },
      {
        "campaignStage": "FORT_KNOBS",
        "loot": 5000
      },
      {
        "campaignStage": "WATCHTOWER",
        "loot": 5000
      },
      {
        "campaignStage": "FOOLS_GOLD",
        "loot": 5000
      },
      {
        "campaignStage": "THOROUGHFARE",
        "loot": 6000
      },
      {
        "campaignStage": "BOUNCY_CASTLE",
        "loot": 6000
      },
      {
        "campaignStage": "FIKOVA",
        "loot": 6000
      },
      {
        "campaignStage": "GOBBO_CAMPUS",
        "loot": 7000
      },
      {
        "campaignStage": "DANNY_BOY",
        "loot": 7000
      },
      {
        "campaignStage": "OMMAHHA_BEECH",
        "loot": 10000
      },
      {
        "campaignStage": "WALLS_OF_STEEL",
        "loot": 10000
      },
      {
        "campaignStage": "SICILIAN_DEFENSE",
        "loot": 10000
      },
      {
        "campaignStage": "OBSIDIAN_TOWER",
        "loot": 15000
      },
      {
        "campaignStage": "ARROW_HEAD",
        "loot": 15000
      },
      {
        "campaignStage": "RED_CARPET",
        "loot": 15000
      },
      {
        "campaignStage": "NATURAL_DEFENSE",
        "loot": 20000
      },
      {
        "campaignStage": "STEEL_GAUNTLET",
        "loot": 20000
      },
      {
        "campaignStage": "QUEENS_GAMBIT",
        "loot": 20000
      },
      {
        "campaignStage": "FULL_FRONTAL",
        "loot": 30000
      },
      {
        "campaignStage": "CHIMP_IN_ARMOR",
        "loot": 30000
      },
      {
        "campaignStage": "FAULTY_TOWERS",
        "loot": 30000
      },
      {
        "campaignStage": "POINT_MAN",
        "loot": 50000
      },
      {
        "campaignStage": "TRIPLE_A",
        "loot": 100000
      },
      {
        "campaignStage": "GOBLIN_PICNIC",
        "loot": 100000
      },
      {
        "campaignStage": "BAIT_N_SWITCH",
        "loot": 100000
      },
      {
        "campaignStage": "COLLATERAL_DAMAGE",
        "loot": 150000
      },
      {
        "campaignStage": "CHOOSE_WISELY",
        "loot": 150000
      },
      {
        "campaignStage": "MEGA_EVIL",
        "loot": 150000
      },
      {
        "campaignStage": "CRYSTAL_CRUST",
        "loot": 200000
      },
      {
        "campaignStage": "COLD_AS_ICE",
        "loot": 250000
      },
      {
        "campaignStage": "JUMP_AROUND",
        "loot": 300000
      },
      {
        "campaignStage": "KITCHEN_SINK",
        "loot": 400000
      },
      {
        "campaignStage": "ROLLING_TERROR",
        "loot": 500000
      },
      {
        "campaignStage": "MEGAMANSION",
        "loot": 600000
      },
      {
        "campaignStage": "PEKKAS_PLAYHOUSE",
        "loot": 700000
      },
      {
        "campaignStage": "SHERBET_TOWERS",
        "loot": 800000
      }
    ],
    "campaignElixirLoot": [
      {
        "campaignStage": "PAYBACK",
        "loot": 500
      },
      {
        "campaignStage": "GOBLIN_FOREST",
        "loot": 500
      },
      {
        "campaignStage": "GOBLIN_OUTPOST",
        "loot": 500
      },
      {
        "campaignStage": "ROCKY_FORT",
        "loot": 1000
      },
      {
        "campaignStage": "GOBLIN_GAUNTLET",
        "loot": 1000
      },
      {
        "campaignStage": "CANNONBALL_RUN",
        "loot": 1000
      },
      {
        "campaignStage": "TWO_SMOKING_BARRELS",
        "loot": 2000
      },
      {
        "campaignStage": "GOLD_RUSH",
        "loot": 2000
      },
      {
        "campaignStage": "MAGINOT_LINE",
        "loot": 2000
      },
      {
        "campaignStage": "RAT_VALLEY",
        "loot": 3000
      },
      {
        "campaignStage": "BRUTE_FORCE",
        "loot": 3000
      },
      {
        "campaignStage": "GOBBOTOWN",
        "loot": 3000
      },
      {
        "campaignStage": "M_IS_FOR_MORTAR",
        "loot": 4000
      },
      {
        "campaignStage": "MEGABLASTER",
        "loot": 4000
      },
      {
        "campaignStage": "IMMOVABLE_OBJECT",
        "loot": 4000
      },
      {
        "campaignStage": "FORT_KNOBS",
        "loot": 5000
      },
      {
        "campaignStage": "WATCHTOWER",
        "loot": 5000
      },
      {
        "campaignStage": "FOOLS_GOLD",
        "loot": 5000
      },
      {
        "campaignStage": "THOROUGHFARE",
        "loot": 6000
      },
      {
        "campaignStage": "BOUNCY_CASTLE",
        "loot": 6000
      },
      {
        "campaignStage": "FIKOVA",
        "loot": 6000
      },
      {
        "campaignStage": "GOBBO_CAMPUS",
        "loot": 7000
      },
      {
        "campaignStage": "DANNY_BOY",
        "loot": 7000
      },
      {
        "campaignStage": "OMMAHHA_BEECH",
        "loot": 10000
      },
      {
        "campaignStage": "WALLS_OF_STEEL",
        "loot": 10000
      },
      {
        "campaignStage": "SICILIAN_DEFENSE",
        "loot": 10000
      },
      {
        "campaignStage": "OBSIDIAN_TOWER",
        "loot": 15000
      },
      {
        "campaignStage": "ARROW_HEAD",
        "loot": 15000
      },
      {
        "campaignStage": "RED_CARPET",
        "loot": 15000
      },
      {
        "campaignStage": "NATURAL_DEFENSE",
        "loot": 20000
      },
      {
        "campaignStage": "STEEL_GAUNTLET",
        "loot": 20000
      },
      {
        "campaignStage": "QUEENS_GAMBIT",
        "loot": 20000
      },
      {
        "campaignStage": "FULL_FRONTAL",
        "loot": 30000
      },
      {
        "campaignStage": "CHIMP_IN_ARMOR",
        "loot": 30000
      },
      {
        "campaignStage": "FAULTY_TOWERS",
        "loot": 30000
      },
      {
        "campaignStage": "POINT_MAN",
        "loot": 50000
      },
      {
        "campaignStage": "TRIPLE_A",
        "loot": 100000
      },
      {
        "campaignStage": "GOBLIN_PICNIC",
        "loot": 100000
      },
      {
        "campaignStage": "BAIT_N_SWITCH",
        "loot": 100000
      },
      {
        "campaignStage": "COLLATERAL_DAMAGE",
        "loot": 150000
      },
      {
        "campaignStage": "CHOOSE_WISELY",
        "loot": 150000
      },
      {
        "campaignStage": "MEGA_EVIL",
        "loot": 150000
      },
      {
        "campaignStage": "CRYSTAL_CRUST",
        "loot": 200000
      },
      {
        "campaignStage": "COLD_AS_ICE",
        "loot": 250000
      },
      {
        "campaignStage": "JUMP_AROUND",
        "loot": 300000
      },
      {
        "campaignStage": "KITCHEN_SINK",
        "loot": 400000
      },
      {
        "campaignStage": "ROLLING_TERROR",
        "loot": 500000
      },
      {
        "campaignStage": "MEGAMANSION",
        "loot": 600000
      },
      {
        "campaignStage": "PEKKAS_PLAYHOUSE",
        "loot": 700000
      },
      {
        "campaignStage": "SHERBET_TOWERS",
        "loot": 800000
      }
    ],
    "givenClanTroops": 2281,
    "receivedClanTroops": 738
  },
  "request": {
    "timeTaken": 51,
    "tag": 22,
    "fromCache": false
  }
}
```

# Notes
 - A common cause for receiving a timeout error (504 Gateway Timeout) from this command is that the given `id` and/or `homeId` do not match an existing player.
 - For a large percentage of players `id` == `homeId`, though there are special cases where the values do not match, and so it is best to always specify the `homeId` along with `id`, if possible.

