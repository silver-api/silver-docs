﻿`warStars` Command
=========
Returns a list containing the war stars for each member in a given clan.

# Parameters
| **Name** | **Type** | **Description**                                                      | **Notes**                                                                                                                                                                 |
|----------|----------|----------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `clanId` | `id`     | Clan from which the current war-stars list should be retrieved from. | For this command to return, the given `clanId` must be registered to a callback that's being actively monitored. See [Callbacks](../callback/callbacks.md) for more info. |


# Example
```
http://127.0.0.1/api/warStars?clanId=103080058097
```
```json
{
  "warStarsList": [
    {
      "userId": 73014585410,
      "homeId": 73014585410,
      "userName": "wiiness",
      "warStars": 136
    },
    {
      "userId": 90198696445,
      "homeId": 90198696445,
      "userName": "Ice~*~",
      "warStars": 619
    },
    {
      "userId": 128850987476,
      "homeId": 128850987476,
      "userName": "dan",
      "warStars": 794
    },
    {
      "userId": 146031814028,
      "homeId": 146031814028,
      "userName": "HenTodz",
      "warStars": 353
    },
    {
      "userId": 17181693098,
      "homeId": 17181693098,
      "userName": "grick370",
      "warStars": 274
    },
    {
      "userId": 107374494376,
      "homeId": 107374494376,
      "userName": "Emperor Jogan",
      "warStars": 744
    },
    {
      "userId": 201863973377,
      "homeId": 201863973377,
      "userName": "ilir",
      "warStars": 808
    },
    {
      "userId": 98785370647,
      "homeId": 98785370647,
      "userName": "Ray.GetaGrip",
      "warStars": 279
    },
    {
      "userId": 167512785962,
      "homeId": 167512785962,
      "userName": "..EMMCC..",
      "warStars": 287
    },
    {
      "userId": 584022,
      "homeId": 584022,
      "userName": "King of Sweden",
      "warStars": 321
    },
    {
      "userId": 249116895636,
      "homeId": 249116895636,
      "userName": "al",
      "warStars": 465
    },
    {
      "userId": 163211784216,
      "homeId": 163211784216,
      "userName": "bearco",
      "warStars": 718
    },
    {
      "userId": 4184041,
      "homeId": 4184041,
      "userName": "Maracle",
      "warStars": 229
    },
    {
      "userId": 21480699645,
      "homeId": 21480699645,
      "userName": "HenTods",
      "warStars": 485
    },
    {
      "userId": 115968433273,
      "homeId": 115968433273,
      "userName": "bear it down",
      "warStars": 420
    },
    {
      "userId": 64425036079,
      "homeId": 64425036079,
      "userName": "Dhaval",
      "warStars": 322
    },
    {
      "userId": 231940336293,
      "homeId": 231940336293,
      "userName": "♂SilverDrake™",
      "warStars": 194
    },
    {
      "userId": 47247898019,
      "homeId": 47247898019,
      "userName": "MitchellJ72",
      "warStars": 135
    },
    {
      "userId": 34362943951,
      "homeId": 34362943951,
      "userName": "RealBrave",
      "warStars": 160
    },
    {
      "userId": 124559052044,
      "homeId": 124559052044,
      "userName": "Ninjy Al",
      "warStars": 345
    },
    {
      "userId": 279182444791,
      "homeId": 279182444791,
      "userName": "III",
      "warStars": 98
    },
    {
      "userId": 146029770107,
      "homeId": 146029770107,
      "userName": "coop",
      "warStars": 678
    },
    {
      "userId": 51547672989,
      "homeId": 51547672988,
      "userName": "infixion",
      "warStars": 170
    },
    {
      "userId": 163212122676,
      "homeId": 163212122676,
      "userName": "Josh G",
      "warStars": 365
    },
    {
      "userId": 158914224447,
      "homeId": 158914224447,
      "userName": "Uli",
      "warStars": 188
    },
    {
      "userId": 274880983869,
      "homeId": 274880983869,
      "userName": "RaHall",
      "warStars": 804
    },
    {
      "userId": 77319160048,
      "homeId": 77319160048,
      "userName": "Ice WarLord",
      "warStars": 562
    },
    {
      "userId": 253412655670,
      "homeId": 253412655670,
      "userName": "whiskeygirl",
      "warStars": 167
    },
    {
      "userId": 167517995136,
      "homeId": 167517995136,
      "userName": "Lawrence",
      "warStars": 12
    },
    {
      "userId": 8605071240,
      "homeId": 8605071240,
      "userName": "frankdadank",
      "warStars": 0
    }
  ],
  "request": {
    "timeTaken": 0,
    "tag": 4,
    "fromCache": false
  }
}
```

# Notes
 - The `warStars` command does not make any requests from the Clash of Clans server, but is instead updated based upon an internal client state that Silver keeps track of. Thus, the `timeTaken` value will always be essentially `0`.
    - While this command could then be considered to return from a cache (`fromCache`), the flag will always be false here. This is simply to reflect the fact that war-star counts are always updated.
 - This command can be replicated by calling [`userInfo`](userInfo.md) for each member in a clan, and retrieving the value of the `WAR_HERO_3` achievement in `userInfo.achievementProgress[]`. While such a replica would work fine, the latency would be thousands of times higher than calling just the `warStars` command.
   - This is because, as mentioned above, Silver keeps track of war attacks as they happen and updates clan members' stars accordingly, rather than checking each time the command is called.
   - Such a replica may be appropriate to use if you cannot establish a callback for a specific clan, though should only be performed periodically.
 - This command is a cheap way of quickly returning the list of players in a callback-registered clan: it's realtime, compact, and very fast.
