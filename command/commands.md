﻿Web-API Commands
=========
Silver exposes a web-API (as defined by the [Configuration](../config/config.md) `server` object) which allows users to query information from the Clash of Clans server via HTTP, and receive the response in [JSON](http://json.org)-format.

Commands are accessed through the defined host and port (`server.ip` and `server.port`), under the `server.basePath` sub-directory. Assuming the default settings, with a port of 80 and base of `'/api/'`, a command request would look something like this:

```
http://127.0.0.1:80/api/clanSearch?name=Ha&region=ITALY&warFrequency=ALWAYS
```
This example would return a list of clans matching against the following criteria:
```json
{
  "name": "Ha",
  "region": "ITALY",
  "warFrequency": "ALWAYS"
}
```

Thus, it can be seen that commands follow the general [query string](https://en.wikipedia.org/wiki/Query_string) format:

`http://` + `server.ip` + `:server.port` + `server.basePath` + `commandName` + `?param1=val1` + `&param2=val2` + `...`

# Command List

| **Name**                        | **Description**                                                                             | **Notes**                                                                                              |
|---------------------------------|---------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| [`userInfo`](userInfo.md)       | Returns profile-information for the specified player.                                       |                                                                                                        |
| [`userVillage`](userVillage.md) | Returns both profile-information and village-data for the specified player.                 |                                                                                                        |
| [`clanInfo`](clanInfo.md)       | Returns information regarding a clan and its members.                                       |                                                                                                        |
| [`clanSearch`](clanSearch.md)   | Searches for clans matching the given criteria, then returns the retrieved list.            |                                                                                                        |
| [`warInfo`](warInfo.md)         | Returns information detailing the specified clan's current war.                             | Usable only with a [callback](../callback/callbacks.md) matching the given `clanId`.                   |
| [`warHistory`](warHistory.md)   | Returns information detailing only the attack-history for the specified clan's current war. | Usable only with a [callback](../callback/callbacks.md) matching the given `clanId`.                   |
| [`warStars`](warStars.md)       | Returns a list containing the war stars for each member in a given clan.                    | Usable only with a [callback](../callback/callbacks.md) matching the given `clanId`.                   |
| [`info`](info.md)               | Returns diagnostic information regarding Silver.                                            |                                                                                                        |
| [`shutdown`](shutdown.md)       | Terminates Silver after a five second delay.                                                | Disabled in config by default.                                                                         |
| `cachedbg`                      | Returns diagnostic information regarding the state of Silver's internal cache.              | Available only in debug-build. Returns a potentially massive amount of data, thus disabled by default. |

# Request Protocol
All calls to Silver API should be in the form of HTTP GET requests. HTTPS is currently unsupported, with no plans to implement such support either.

&nbsp;

Parameters are generally unprocessed and interpreted as whatever type might be needed in a certain scenario. However, values prefixed with `!` or `%23` (URL-encoded '#') may be used in place of any numeric long-integer ID, allowing the usage of the user and clan-tags which Clash of Clans displays in-game.

&nbsp;

| **Name** | **Definition**                                 | **Notes**                                                                                                                                   |
|----------|------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
| `bool`   | `true` or `false` (OR 1/0)                     | `false` corresponds to 0, while `true` is 1.                                                                                                |
| `int`    | -2,147,483,648 to +2,147,483,647               |                                                                                                                                             |
| `ulong`  | 0 to +18,446,744,073,709,551,615               | Used for IDs.                                                                                                                               |
| `tagId`  | `!` or `%23` followed by a user/clan-tag       | Used for IDs. In-game display style for Clash of Clans.                                                                                     |
| `id`     | A `ulong` OR `tagId` value.                    | Example: either `!9Y8YJQVY` or 98785635249 can be used, but both refer to the same player. The first is a `tagId`, the second is a `ulong`. |
| `string` | An UTF-8 encoded string (array of characters). | Please don't quote around strings, just make sure things are properly [URL-encoded](https://en.wikipedia.org/wiki/Percent-encoding).        |

# Response Format
All responses provided by Silver will be [JSON](http://json.org)-encoded, though with all extra spacing removed to reduce transmission size (i.e., the output isn't pretty-printed). Because of this, it is suggested to use a formatter such as [JSON Editor Online](http://jsoneditoronline.org), or an auto-formatting browser such as [Firefox Nightly](https://nightly.mozilla.org/), when manually examining the responses provided.

The lack of pretty-printing has no effect on proper JSON parsers, as used in Ruby/PHP/JavaScript/etc.

Appended to each response is a `request` object, which includes some details regarding the fulfilled request:
```json
"request": {
  "timeTaken": 49,
  "tag": 11,
  "fromCache": false
}
```
| **Name**    | **Description**                                                                                                   | **Notes**                                                                                               |
|-------------|-------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| `timeTaken` | The duration between a client issuing the request and producing output from the Clash of Clans server's response. | Measured in milliseconds. Equal to `time waiting on server's response` + `time spent parsing response`. |
| `tag`       | The tag which uniquely identifies this request.                                                                   | Used in debugging.                                                                                      |
| `fromCache` | Indicates whether or not the response was retrieved from Silver's cache, rather than the Clash of Clans server.   | Used in debugging and as general info.                                                                  |


