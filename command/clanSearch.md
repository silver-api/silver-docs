﻿`clanSearch` Command
=========
Searches for clans matching the given criteria, then returns the retrieved list.

# Parameters
| **Name**         | **Type**                                      | **Description**                        | **Notes**                      |
|------------------|-----------------------------------------------|----------------------------------------|--------------------------------|
| `name`           | `string`                                      | Clan-name to search for.               | Not a completely strict match. |
| *`warFrequency`* | *[`EWarFrequency`](../types/warFrequency.md)* | *Desired war-frequency.*               | *Optional. Default: ANY.*      |
| *`region`*       | *[`ERegion`](../types/region.md)*             | *Desired clan region.*                 | *Optional. Default: ANY.*      |
| *`minMembers`*   | *`int` (1–50)*                                | *Minimum clan-member count.*           | *Optional. Default: 1.*        |
| *`maxMembers`*   | *`int` (1–50)*                                | *Maximum clan-member count.*           | *Optional. Default: 50.*       |
| *`minScore`*     | *`int`*                                       | *Minimum clan-score.*                  | *Optional. Default: 0.*        |
| *`minLevel`*     | *`int`*                                       | *Minimum clan-level.*                  | *Optional. Default: 1.*        |
| *`isJoinable`*   | *`bool`*                                      | *Show only clans the client can join.* | *Optional. Default: `false`.*  |


# Example
```
http://127.0.0.1/api/clanSearch?name=Carrot%20Cake&region=NORTH_KOREA&minScore=555
```
```json
{
  "searchResults": [
    {
      "id": 180390230436,
      "name": "carrot cake",
      "badge": 13000015,
      "status": "ANYONE_CAN_JOIN",
      "memberCount": 1,
      "score": 624,
      "requiredTrophies": 800,
      "warsWon": 0,
      "warsLost": 0,
      "warsTied": 0,
      "language": "EN",
      "warFrequency": "ALWAYS",
      "region": "NORTH_KOREA",
      "level": 1,
      "warWinStreak": 0,
      "hasPublicWarLog": true
    },
    {
      "id": 176095591279,
      "name": "Cakes R us",
      "badge": 1627396694,
      "status": "ANYONE_CAN_JOIN",
      "memberCount": 4,
      "score": 2730,
      "requiredTrophies": 0,
      "warsWon": 10,
      "warsLost": 14,
      "warsTied": 0,
      "language": "EN",
      "warFrequency": "TWICE_A_WEEK",
      "region": "NORTH_KOREA",
      "level": 3,
      "warWinStreak": 0,
      "hasPublicWarLog": true
    },
    {
      "id": 193274635578,
      "name": "layer cake",
      "badge": 1526729304,
      "status": "INVITE_ONLY",
      "memberCount": 2,
      "score": 1039,
      "requiredTrophies": 1000,
      "warsWon": 0,
      "warsLost": 0,
      "warsTied": 0,
      "language": "EN",
      "warFrequency": 5,
      "region": "NORTH_KOREA",
      "level": 1,
      "warWinStreak": 0,
      "hasPublicWarLog": true
    }
  ],
  "request": {
    "timeTaken": 118,
    "tag": 34,
    "fromCache": false
  }
}
```

# Notes
 - When specifying the `minScore` and `maxScore` parameters, it's important to ensure that `maxScore` is always greater than or equal to `minScore`. Otherwise the API will return an error-response.
   - Additionally, both parameters must be within range 1–50 or an error-response will be returned instead.
 - While `minScore` is typically limited to increments of 100 in-game, any positive value works fine through Silver, even if it's not a multiple of 100.
 - As mentioned in the parameters section, the `name` field does not perform an exact match, but rather checks for similarity.
