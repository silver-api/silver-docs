﻿`warInfo` Command
=========
Returns information detailing the specified clan's current war.

# Parameters
| **Name** | **Type** | **Description**                                                | **Notes**                                                                                                                                                                 |
|----------|----------|----------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `clanId` | `id`     | Clan from which the current war-info should be retrieved from. | For this command to return, the given `clanId` must be registered to a callback that's being actively monitored. See [Callbacks](../callback/callbacks.md) for more info. |


# Example
```
http://127.0.0.1/api/warInfo?clanId=103080058097
```
```json
{
  "warInfo": {
    "warState": "BATTLE_DAY",
    "secondsRemaining": 77527,
    "allyClan": {
      "clanId": 103080058097,
      "clanName": "The Renegades",
      "clanBadge": 1778392666,
      "clanLevel": 9,
      "warPlayers": [
        {
          "clanId": 103080058097,
          "userId": 73014585410,
          "homeId": 73014585410,
          "userName": "wiiness",
          "attacksUsedCount": 1,
          "goldWeight": 87000,
          "elixirWeight": 87000,
          "darkElixirWeight": 470,
          "townHallLevel": 10,
          "warPosition": 0,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "GOLEM",
                  "level": 5
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 128850987476,
          "homeId": 128850987476,
          "userName": "dan",
          "attacksUsedCount": 0,
          "goldWeight": 83000,
          "elixirWeight": 83000,
          "darkElixirWeight": 440,
          "townHallLevel": 10,
          "warPosition": 1,
          "warId": 206161777365,
          "clanCastleLevel": 6,
          "clanCastleCapacity": 35,
          "troopRequestMessage": "max golem and 5 archers",
          "clanCastleUsedSpace": 35,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "GOLEM",
                  "level": 5
                }
              ]
            },
            {
              "donorId": 201863973377,
              "troops": [
                {
                  "type": "ARCHER",
                  "level": 7
                },
                {
                  "type": "ARCHER",
                  "level": 7
                },
                {
                  "type": "ARCHER",
                  "level": 7
                },
                {
                  "type": "ARCHER",
                  "level": 7
                },
                {
                  "type": "ARCHER",
                  "level": 7
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 201863973377,
          "homeId": 201863973377,
          "userName": "ilir",
          "attacksUsedCount": 1,
          "goldWeight": 70000,
          "elixirWeight": 70000,
          "darkElixirWeight": 330,
          "townHallLevel": 9,
          "warPosition": 2,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "max golem plz",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "GOLEM",
                  "level": 5
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 163211784216,
          "homeId": 163211784216,
          "userName": "bearco",
          "attacksUsedCount": 1,
          "goldWeight": 69000,
          "elixirWeight": 69000,
          "darkElixirWeight": 320,
          "townHallLevel": 9,
          "warPosition": 3,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": [
            {
              "donorId": 201863973377,
              "troops": [
                {
                  "type": "DRAGON",
                  "level": 5
                },
                {
                  "type": "BALLOON",
                  "level": 7
                },
                {
                  "type": "BALLOON",
                  "level": 7
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 115968433273,
          "homeId": 115968433273,
          "userName": "bear it down",
          "attacksUsedCount": 2,
          "goldWeight": 64000,
          "elixirWeight": 64000,
          "darkElixirWeight": 280,
          "townHallLevel": 9,
          "warPosition": 4,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "max golem",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "GOLEM",
                  "level": 5
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 249116895636,
          "homeId": 249116895636,
          "userName": "al",
          "attacksUsedCount": 0,
          "goldWeight": 62000,
          "elixirWeight": 62000,
          "darkElixirWeight": 270,
          "townHallLevel": 9,
          "warPosition": 5,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "DRAGON",
                  "level": 5
                },
                {
                  "type": "BALLOON",
                  "level": 7
                },
                {
                  "type": "BALLOON",
                  "level": 7
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 146029770107,
          "homeId": 146029770107,
          "userName": "coop",
          "attacksUsedCount": 0,
          "goldWeight": 62000,
          "elixirWeight": 62000,
          "darkElixirWeight": 260,
          "townHallLevel": 9,
          "warPosition": 6,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "1 baby drag 1 witch 1 valk",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "BABY_DRAGON",
                  "level": 3
                }
              ]
            },
            {
              "donorId": 201863973377,
              "troops": [
                {
                  "type": "WITCH",
                  "level": 3
                },
                {
                  "type": "VALKYRIE",
                  "level": 5
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 274880983869,
          "homeId": 274880983869,
          "userName": "RaHall",
          "attacksUsedCount": 2,
          "goldWeight": 61000,
          "elixirWeight": 61000,
          "darkElixirWeight": 250,
          "townHallLevel": 9,
          "warPosition": 7,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "DRAGON",
                  "level": 5
                },
                {
                  "type": "BALLOON",
                  "level": 7
                },
                {
                  "type": "BALLOON",
                  "level": 7
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 77319160048,
          "homeId": 77319160048,
          "userName": "Ice WarLord",
          "attacksUsedCount": 0,
          "goldWeight": 53000,
          "elixirWeight": 53000,
          "darkElixirWeight": 190,
          "townHallLevel": 8,
          "warPosition": 8,
          "warId": 206161777365,
          "clanCastleLevel": 4,
          "clanCastleCapacity": 25,
          "troopRequestMessage": "max drag loons only",
          "clanCastleUsedSpace": 25,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "DRAGON",
                  "level": 5
                },
                {
                  "type": "BALLOON",
                  "level": 7
                }
              ]
            }
          ]
        },
        {
          "clanId": 103080058097,
          "userId": 253412655670,
          "homeId": 253412655670,
          "userName": "whiskeygirl",
          "attacksUsedCount": 0,
          "goldWeight": 47000,
          "elixirWeight": 47000,
          "darkElixirWeight": 160,
          "townHallLevel": 9,
          "warPosition": 9,
          "warId": 206161777365,
          "clanCastleLevel": 4,
          "clanCastleCapacity": 25,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 25,
          "castleDonationEntries": [
            {
              "donorId": 163211784216,
              "troops": [
                {
                  "type": "DRAGON",
                  "level": 5
                },
                {
                  "type": "BALLOON",
                  "level": 7
                }
              ]
            }
          ]
        }
      ],
      "weights": [
        5,
        5,
        5,
        5,
        5,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2
      ]
    },
    "enemyClan": {
      "clanId": 8591642888,
      "clanName": "부산",
      "clanBadge": 1744844111,
      "clanLevel": 9,
      "warPlayers": [
        {
          "clanId": 8591642888,
          "userId": 12892068573,
          "homeId": 12892068573,
          "userName": "명현",
          "attacksUsedCount": 0,
          "goldWeight": 87000,
          "elixirWeight": 87000,
          "darkElixirWeight": 480,
          "townHallLevel": 10,
          "warPosition": 0,
          "warId": 206161777365,
          "clanCastleLevel": 6,
          "clanCastleCapacity": 35,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 35,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 219046585674,
          "homeId": 219046585674,
          "userName": "코카콜라",
          "attacksUsedCount": 0,
          "goldWeight": 86000,
          "elixirWeight": 86000,
          "darkElixirWeight": 460,
          "townHallLevel": 10,
          "warPosition": 1,
          "warId": 206161777365,
          "clanCastleLevel": 6,
          "clanCastleCapacity": 35,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 35,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 214751012723,
          "homeId": 214751012723,
          "userName": "◈부산자이언츠◈",
          "attacksUsedCount": 0,
          "goldWeight": 75000,
          "elixirWeight": 75000,
          "darkElixirWeight": 370,
          "townHallLevel": 10,
          "warPosition": 2,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 197573109802,
          "homeId": 197573109802,
          "userName": "삐돌이",
          "attacksUsedCount": 1,
          "goldWeight": 65000,
          "elixirWeight": 65000,
          "darkElixirWeight": 290,
          "townHallLevel": 9,
          "warPosition": 3,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 240521410409,
          "homeId": 240521410409,
          "userName": "민이윤이천사",
          "attacksUsedCount": 0,
          "goldWeight": 65000,
          "elixirWeight": 65000,
          "darkElixirWeight": 280,
          "townHallLevel": 9,
          "warPosition": 4,
          "warId": 206161777365,
          "clanCastleLevel": 4,
          "clanCastleCapacity": 25,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 25,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 201874992412,
          "homeId": 201874992412,
          "userName": "하단",
          "attacksUsedCount": 0,
          "goldWeight": 62000,
          "elixirWeight": 62000,
          "darkElixirWeight": 260,
          "townHallLevel": 9,
          "warPosition": 5,
          "warId": 206161777365,
          "clanCastleLevel": 5,
          "clanCastleCapacity": 30,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 30,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 167514647809,
          "homeId": 167514647809,
          "userName": "금사DC",
          "attacksUsedCount": 0,
          "goldWeight": 60000,
          "elixirWeight": 60000,
          "darkElixirWeight": 250,
          "townHallLevel": 9,
          "warPosition": 6,
          "warId": 206161777365,
          "clanCastleLevel": 4,
          "clanCastleCapacity": 25,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 25,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 283468047782,
          "homeId": 283468047782,
          "userName": "네시아",
          "attacksUsedCount": 0,
          "goldWeight": 51000,
          "elixirWeight": 51000,
          "darkElixirWeight": 180,
          "townHallLevel": 9,
          "warPosition": 7,
          "warId": 206161777365,
          "clanCastleLevel": 3,
          "clanCastleCapacity": 20,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 20,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 64436872544,
          "homeId": 64436872544,
          "userName": "ㅎㅎㅎㅎ",
          "attacksUsedCount": 0,
          "goldWeight": 47000,
          "elixirWeight": 47000,
          "darkElixirWeight": 160,
          "townHallLevel": 8,
          "warPosition": 8,
          "warId": 206161777365,
          "clanCastleLevel": 4,
          "clanCastleCapacity": 25,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 25,
          "castleDonationEntries": []
        },
        {
          "clanId": 8591642888,
          "userId": 309241284567,
          "homeId": 309241284567,
          "userName": "지니",
          "attacksUsedCount": 0,
          "goldWeight": 33000,
          "elixirWeight": 33000,
          "darkElixirWeight": 40,
          "townHallLevel": 7,
          "warPosition": 9,
          "warId": 206161777365,
          "clanCastleLevel": 3,
          "clanCastleCapacity": 20,
          "troopRequestMessage": "",
          "clanCastleUsedSpace": 20,
          "castleDonationEntries": []
        }
      ],
      "weights": [
        5,
        5,
        5,
        5,
        5,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        4,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        3,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2,
        2
      ]
    },
    "warId": 206161777365,
    "attackHistory": [
      {
        "warSecondsRemaining": 79001,
        "attackerClanId": 103080058097,
        "attackerId": 73014585410,
        "defenderClanId": 8591642888,
        "defenderId": 12892068573,
        "attackerName": "wiiness",
        "defenderName": "명현",
        "starsGained": 1,
        "newStarsGained": 1,
        "percentRating": 37,
        "battleDuration": 70
      },
      {
        "warSecondsRemaining": 83588,
        "attackerClanId": 103080058097,
        "attackerId": 163211784216,
        "defenderClanId": 8591642888,
        "defenderId": 197573109802,
        "attackerName": "bearco",
        "defenderName": "삐돌이",
        "starsGained": 3,
        "newStarsGained": 3,
        "percentRating": 100,
        "battleDuration": 140
      },
      {
        "warSecondsRemaining": 84026,
        "attackerClanId": 103080058097,
        "attackerId": 274880983869,
        "defenderClanId": 8591642888,
        "defenderId": 167514647809,
        "attackerName": "RaHall",
        "defenderName": "금사DC",
        "starsGained": 2,
        "newStarsGained": 2,
        "percentRating": 71,
        "battleDuration": 179
      },
      {
        "warSecondsRemaining": 84883,
        "attackerClanId": 103080058097,
        "attackerId": 115968433273,
        "defenderClanId": 8591642888,
        "defenderId": 201874992412,
        "attackerName": "bear it down",
        "defenderName": "하단",
        "starsGained": 3,
        "newStarsGained": 3,
        "percentRating": 100,
        "battleDuration": 150
      },
      {
        "warSecondsRemaining": 85200,
        "attackerClanId": 103080058097,
        "attackerId": 201863973377,
        "defenderClanId": 8591642888,
        "defenderId": 214751012723,
        "attackerName": "ilir",
        "defenderName": "◈부산자이언츠◈",
        "starsGained": 0,
        "newStarsGained": 0,
        "percentRating": 49,
        "battleDuration": 85
      },
      {
        "warSecondsRemaining": 85506,
        "attackerClanId": 8591642888,
        "attackerId": 197573109802,
        "defenderClanId": 103080058097,
        "defenderId": 274880983869,
        "attackerName": "삐돌이",
        "defenderName": "RaHall",
        "starsGained": 2,
        "newStarsGained": 2,
        "percentRating": 65,
        "battleDuration": 114
      },
      {
        "warSecondsRemaining": 85765,
        "attackerClanId": 103080058097,
        "attackerId": 115968433273,
        "defenderClanId": 8591642888,
        "defenderId": 240521410409,
        "attackerName": "bear it down",
        "defenderName": "민이윤이천사",
        "starsGained": 2,
        "newStarsGained": 2,
        "percentRating": 99,
        "battleDuration": 179
      },
      {
        "warSecondsRemaining": 86052,
        "attackerClanId": 103080058097,
        "attackerId": 274880983869,
        "defenderClanId": 8591642888,
        "defenderId": 283468047782,
        "attackerName": "RaHall",
        "defenderName": "네시아",
        "starsGained": 3,
        "newStarsGained": 3,
        "percentRating": 100,
        "battleDuration": 110
      }
    ]
  },
  "request": {
    "timeTaken": 0,
    "tag": 1,
    "fromCache": false
  }
}
```

# Notes
 - The `warInfo` command does not make any requests from the Clash of Clans server, but is instead updated based upon an internal client state that Silver keeps track of. Thus, the `timeTaken` value will always be essentially `0`.
    - While this command could then be considered to return from a cache (`fromCache`), the flag will always be false here. This is simply to reflect the fact that war info is always updated.
 - All `battleDuration` values are measured in seconds.
 - The `secondsRemaining` value is realtime, meaning the value is updated and accurate for the current value upon return. 
 - If only the war history is needed, without the entire player list and clan information, one should call [`warHistory`](warHistory.md) instead.
 