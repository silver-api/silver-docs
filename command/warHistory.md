﻿`warHistory` Command
=========
Returns information detailing only the attack-history for the specified clan's current war.

# Parameters
| **Name** | **Type** | **Description**                                                | **Notes**                                                                                                                                                                 |
|----------|----------|----------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `clanId` | `id`     | Clan from which the current war-history should be retrieved from. | For this command to return, the given `clanId` must be registered to a callback that's being actively monitored. See [Callbacks](../callback/callbacks.md) for more info. |


# Example
```
http://127.0.0.1/api/warHistory?clanId=103080058097
```
```json
{
  "secondsRemaining": 77526,
  "attackHistory": [
    {
      "warSecondsRemaining": 79001,
      "attackerClanId": 103080058097,
      "attackerId": 73014585410,
      "defenderClanId": 8591642888,
      "defenderId": 12892068573,
      "attackerName": "wiiness",
      "defenderName": "명현",
      "starsGained": 1,
      "newStarsGained": 1,
      "percentRating": 37,
      "battleDuration": 70
    },
    {
      "warSecondsRemaining": 83588,
      "attackerClanId": 103080058097,
      "attackerId": 163211784216,
      "defenderClanId": 8591642888,
      "defenderId": 197573109802,
      "attackerName": "bearco",
      "defenderName": "삐돌이",
      "starsGained": 3,
      "newStarsGained": 3,
      "percentRating": 100,
      "battleDuration": 140
    },
    {
      "warSecondsRemaining": 84026,
      "attackerClanId": 103080058097,
      "attackerId": 274880983869,
      "defenderClanId": 8591642888,
      "defenderId": 167514647809,
      "attackerName": "RaHall",
      "defenderName": "금사DC",
      "starsGained": 2,
      "newStarsGained": 2,
      "percentRating": 71,
      "battleDuration": 179
    },
    {
      "warSecondsRemaining": 84883,
      "attackerClanId": 103080058097,
      "attackerId": 115968433273,
      "defenderClanId": 8591642888,
      "defenderId": 201874992412,
      "attackerName": "bear it down",
      "defenderName": "하단",
      "starsGained": 3,
      "newStarsGained": 3,
      "percentRating": 100,
      "battleDuration": 150
    },
    {
      "warSecondsRemaining": 85200,
      "attackerClanId": 103080058097,
      "attackerId": 201863973377,
      "defenderClanId": 8591642888,
      "defenderId": 214751012723,
      "attackerName": "ilir",
      "defenderName": "◈부산자이언츠◈",
      "starsGained": 0,
      "newStarsGained": 0,
      "percentRating": 49,
      "battleDuration": 85
    },
    {
      "warSecondsRemaining": 85506,
      "attackerClanId": 8591642888,
      "attackerId": 197573109802,
      "defenderClanId": 103080058097,
      "defenderId": 274880983869,
      "attackerName": "삐돌이",
      "defenderName": "RaHall",
      "starsGained": 2,
      "newStarsGained": 2,
      "percentRating": 65,
      "battleDuration": 114
    },
    {
      "warSecondsRemaining": 85765,
      "attackerClanId": 103080058097,
      "attackerId": 115968433273,
      "defenderClanId": 8591642888,
      "defenderId": 240521410409,
      "attackerName": "bear it down",
      "defenderName": "민이윤이천사",
      "starsGained": 2,
      "newStarsGained": 2,
      "percentRating": 99,
      "battleDuration": 179
    },
    {
      "warSecondsRemaining": 86052,
      "attackerClanId": 103080058097,
      "attackerId": 274880983869,
      "defenderClanId": 8591642888,
      "defenderId": 283468047782,
      "attackerName": "RaHall",
      "defenderName": "네시아",
      "starsGained": 3,
      "newStarsGained": 3,
      "percentRating": 100,
      "battleDuration": 110
    }
  ],
  "request": {
    "timeTaken": 0,
    "tag": 3,
    "fromCache": false
  }
}
```

# Notes
 - The `warHistory` command does not make any requests from the Clash of Clans server, but is instead updated based upon an internal client state that Silver keeps track of. Thus, the `timeTaken` value will always be essentially `0`.
    - While this command could then be considered to return from a cache (`fromCache`), the flag will always be false here. This is simply to reflect the fact that the war history info is always updated.
 - All `battleDuration` values are measured in seconds.
 - The `secondsRemaining` value is realtime, meaning the value is updated and accurate for the current value upon return.
 - If one requires the clan info and war participants list, along with some other additional data, the [`warInfo`](warInfo.md) command should be used instead.
 