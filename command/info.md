﻿`info` Command
=========
Returns diagnostic information regarding Silver.

# Parameters
This command has no parameters.


# Example
```
http://127.0.0.1/api/info
```
```json
{
  "request": {
    "timeTaken": 0,
    "tag": 0,
    "fromCache": false
  },
  "version": {
    "codeName": "Fox",
    "verMajor": 0,
    "verMinor": 1,
    "revision": 3,
    "build": 730
  },
  "clients": {
    "active": [
      {
        "displayName": "Client 1",
        "exposedName": "c1",
        "requestsHandled": 0
      }
    ],
    "reserve": [
      {
        "displayName": "Client 2",
        "exposedName": "c2",
        "requestsHandled": 0
      }
    ]
  },
  "uptime": 8699,
  "engine": "SILVER"
}
```

# Notes
 - Valid `engine` values are: `SILVER` and `DESILVER`, indicating normal or debug use, accordingly.
 - This command does not require any active clients to run, thus can be used to check the state of things regardless of client condition.
 - This command *cannot* be forwarded.
