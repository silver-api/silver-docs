﻿`shutdown` Command
=========
Terminates Silver after a five second delay.

# Parameters
This command has no parameters.


# Example
```
http://127.0.0.1/api/shutdown
```
```json
{
  "request": {
    "timeTaken": 0,
    "tag": 0,
    "fromCache": false
  },
  "status": "OK! Killing server in 5 seconds..."
}
```

# Notes
 - This command is disabled by default. It can be enabled within `config.json`, though this is not recommended unless you have an established reset mechanism and valid reason for doing so.
 - This command *cannot* be forwarded.
