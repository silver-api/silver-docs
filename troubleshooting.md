﻿Troubleshooting
=========
Below is a list of possible issues, and their resolutions. If any issue you happen to encounter isn't listed below, please visit our [Skype group](https://join.skype.com/BZCFlDbztzKg).

# [Error] Unable to startup web-server!
Something is almost certainly using port-80. Skype can do this for some connections, so it might be worth closing Skype and trying to launch Silver once more. Alternatively, disable the following option in Skype: *Tools -> Options -> Advanced -> Connection -> Use port 80 and 443 for additional incoming connections*

If Skype isn't causing the issue, try running `netstat -a -b` in an elevated console on Windows, or `lsof -iTCP -sTCP:LISTEN` in Linux. Try and find the name of the process using port-80 from here, and close it if possible.

# The web-API is returning nonsense
If you're receiving random-looking useless data from the web-API, it's likely due to using `file_get_contents` on an old version of PHP, while the `server.useDeflate` flag is enabled. This option compresses all returned data using the deflate method, which PHP won't decompress before returning from the `file_get_contents` method.

Luckily, `file_get_contents` sucks and you shouldn't be using it. Use [cURL](php.net/manual/book.curl.php) instead. Alternatively, you can disable deflate-compression by setting `server.useDeflate` to false within `config.json`. It is best to leave compression on though, as it can greatly reduce bandwidth usage and result in speedier responses.

```php
<?php
$curl = curl_init('127.0.0.1/api/userInfo?id=!9Y8YJQVY');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT_MS, 10000); 
curl_setopt($curl, CURLOPT_TIMEOUT_MS, 10000);
curl_setopt($curl, CURLOPT_ENCODING, 1);		// enables auto-detection of encoding (needed to decompress)

$res = curl_exec($curl);
curl_close($curl);

echo $res;
?>
```
*Example using cURL to retreive page contents.*

*-cough- you can actually just call `gzinflate()` on the response from `file_get_contents()` -cough- it still sucks, though -cough-cough- use cURL -cough-*

# My hovercraft is full of eels!
But is it really a problem? ( ͡☉ ͜ʖ ͡☉)
