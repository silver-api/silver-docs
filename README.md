﻿Silver Docs
=========
Welcome to the Silver API documentation repo! Here you can find up-to-date information regarding setup, general configuration, commands, and more. Tap a section below to easily navigate to the corresponding content.

 - [What is Silver?](what_is_silver.md)
 - [Getting Started](getting_started.md)
 - [Configuration](config/config.md)
 - [Commands](command/commands.md)
 - [Callbacks](callback/callbacks.md)
 
Thanks for choosing Silver, and feel free to leave a message on our [Skype group](https://join.skype.com/BZCFlDbztzKg) if you have any questions or concerns!
