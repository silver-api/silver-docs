﻿What is Silver?
=========
Silver is a headless client for Clash of Clans: a minimal-overhead application that's able to create and monitor Clash of Clans sessions. It's capable of providing useful data that's either otherwise unavailable, or that would require significant effort to manually collect.

Silver can help you keep tabs on and manage clans and players in a few different ways:
 
 - **Tracking war status -** with Silver, you can receive up-to-date, realtime information regarding events in your active Clan Wars, providing you with a whole history of the battle as events happen.
 - **Tracking clan changes -** stay updated on clan events and get notified anytime a member leaves, joins, is promoted, or clan settings are modified; all easily provided with Silver callbacks.
 - **Gathering user data -** Silver provides fast and easy calls to retrieve user information, containing achievement progress, village layout info, army composition, and much more... (along with some normally hidden information)

All the while being built with speed and reliability in mind. 

If any of this sounds like it could be of use to you or your clan, stick around and take a look through the [documentation](README.md) or send us a message on our [Skype group](https://join.skype.com/BZCFlDbztzKg) for more information.
