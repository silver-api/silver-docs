﻿Region Enum
=========
| **Name**                         | **Value** |
|----------------------------------|-----------|
| `ANY`                            | 0         |
| `EUROPE`                         | 32000000  |
| `NORTH_AMERICA`                  | 32000001  |
| `SOUTH_AMERICA`                  | 32000002  |
| `ASIA`                           | 32000003  |
| `AUSTRALIA_CONTINENT`            | 32000004  |
| `AFRICA`                         | 32000005  |
| `INTERNATIONAL`                  | 32000006  |
| `AFGHANISTAN`                    | 32000007  |
| `ALAND_ISLANDS`                  | 32000008  |
| `ALBANIA`                        | 32000009  |
| `ALGERIA`                        | 32000010  |
| `AMERICAN_SAMOA`                 | 32000011  |
| `ANDORRA`                        | 32000012  |
| `ANGOLA`                         | 32000013  |
| `ANGUILLA`                       | 32000014  |
| `ANTARCTICA`                     | 32000015  |
| `ANTIGUA_AND_BARBUDA`            | 32000016  |
| `ARGENTINA`                      | 32000017  |
| `ARMENIA`                        | 32000018  |
| `ARUBA`                          | 32000019  |
| `ASCENSION_ISLAND`               | 32000020  |
| `AUSTRALIA`                      | 32000021  |
| `AUSTRIA`                        | 32000022  |
| `AZERBAIJAN`                     | 32000023  |
| `BAHAMAS`                        | 32000024  |
| `BAHRAIN`                        | 32000025  |
| `BANGLADESH`                     | 32000026  |
| `BARBADOS`                       | 32000027  |
| `BELARUS`                        | 32000028  |
| `BELGIUM`                        | 32000029  |
| `BELIZE`                         | 32000030  |
| `BENIN`                          | 32000031  |
| `BERMUDA`                        | 32000032  |
| `BHUTAN`                         | 32000033  |
| `BOLIVIA`                        | 32000034  |
| `BOSNIA_AND_HERZEGOVINA`         | 32000035  |
| `BOTSWANA`                       | 32000036  |
| `BOUVET_ISLAND`                  | 32000037  |
| `BRAZIL`                         | 32000038  |
| `BRITISH_INDIAN_OCEAN_TERRITORY` | 32000039  |
| `BRITISH_VIRGIN_ISLANDS`         | 32000040  |
| `BRUNEI`                         | 32000041  |
| `BULGARIA`                       | 32000042  |
| `BURKINA_FASO`                   | 32000043  |
| `BURUNDI`                        | 32000044  |
| `CAMBODIA`                       | 32000045  |
| `CAMEROON`                       | 32000046  |
| `CANADA`                         | 32000047  |
| `CANARY_ISLANDS`                 | 32000048  |
| `CAPE_VERDE`                     | 32000049  |
| `CARIBBEAN_NETHERLANDS`          | 32000050  |
| `CAYMAN_ISLANDS`                 | 32000051  |
| `CENTRAL_AFRICAN_REPUBLIC`       | 32000052  |
| `CEUTA_AND_MELILLA`              | 32000053  |
| `CHAD`                           | 32000054  |
| `CHILE`                          | 32000055  |
| `CHINA`                          | 32000056  |
| `CHRISTMAS_ISLAND`               | 32000057  |
| `COCOS_KEELING_ISLANDS`          | 32000058  |
| `COLOMBIA`                       | 32000059  |
| `COMOROS`                        | 32000060  |
| `CONGO_DRC`                      | 32000061  |
| `CONGO_REPUBLIC`                 | 32000062  |
| `COOK_ISLANDS`                   | 32000063  |
| `COSTA_RICA`                     | 32000064  |
| `COTE_DIVOIRE`                   | 32000065  |
| `CROATIA`                        | 32000066  |
| `CUBA`                           | 32000067  |
| `CURACAO`                        | 32000068  |
| `CYPRUS`                         | 32000069  |
| `CZECH_REPUBLIC`                 | 32000070  |
| `DENMARK`                        | 32000071  |
| `DIEGO_GARCIA`                   | 32000072  |
| `DJIBOUTI`                       | 32000073  |
| `DOMINICA`                       | 32000074  |
| `DOMINICAN_REPUBLIC`             | 32000075  |
| `ECUADOR`                        | 32000076  |
| `EGYPT`                          | 32000077  |
| `EL_SALVADOR`                    | 32000078  |
| `EQUATORIAL_GUINEA`              | 32000079  |
| `ERITREA`                        | 32000080  |
| `ESTONIA`                        | 32000081  |
| `ETHIOPIA`                       | 32000082  |
| `FALKLAND_ISLANDS`               | 32000083  |
| `FAROE_ISLANDS`                  | 32000084  |
| `FIJI`                           | 32000085  |
| `FINLAND`                        | 32000086  |
| `FRANCE`                         | 32000087  |
| `FRENCH_GUIANA`                  | 32000088  |
| `FRENCH_POLYNESIA`               | 32000089  |
| `FRENCH_SOUTHERN_TERRITORIES`    | 32000090  |
| `GABON`                          | 32000091  |
| `GAMBIA`                         | 32000092  |
| `GEORGIA`                        | 32000093  |
| `GERMANY`                        | 32000094  |
| `GHANA`                          | 32000095  |
| `GIBRALTAR`                      | 32000096  |
| `GREECE`                         | 32000097  |
| `GREENLAND`                      | 32000098  |
| `GRENADA`                        | 32000099  |
| `GUADELOUPE`                     | 32000100  |
| `GUAM`                           | 32000101  |
| `GUATEMALA`                      | 32000102  |
| `GUERNSEY`                       | 32000103  |
| `GUINEA`                         | 32000104  |
| `GUINEA_BISSAU`                  | 32000105  |
| `GUYANA`                         | 32000106  |
| `HAITI`                          | 32000107  |
| `HEARD_AND_MCDONALD_ISLANDS`     | 32000108  |
| `HONDURAS`                       | 32000109  |
| `HONG_KONG`                      | 32000110  |
| `HUNGARY`                        | 32000111  |
| `ICELAND`                        | 32000112  |
| `INDIA`                          | 32000113  |
| `INDONESIA`                      | 32000114  |
| `IRAN`                           | 32000115  |
| `IRAQ`                           | 32000116  |
| `IRELAND`                        | 32000117  |
| `ISLE_OF_MAN`                    | 32000118  |
| `ISRAEL`                         | 32000119  |
| `ITALY`                          | 32000120  |
| `JAMAICA`                        | 32000121  |
| `JAPAN`                          | 32000122  |
| `JERSEY`                         | 32000123  |
| `JORDAN`                         | 32000124  |
| `KAZAKHSTAN`                     | 32000125  |
| `KENYA`                          | 32000126  |
| `KIRIBATI`                       | 32000127  |
| `KOSOVO`                         | 32000128  |
| `KUWAIT`                         | 32000129  |
| `KYRGYZSTAN`                     | 32000130  |
| `LAOS`                           | 32000131  |
| `LATVIA`                         | 32000132  |
| `LEBANON`                        | 32000133  |
| `LESOTHO`                        | 32000134  |
| `LIBERIA`                        | 32000135  |
| `LIBYA`                          | 32000136  |
| `LIECHTENSTEIN`                  | 32000137  |
| `LITHUANIA`                      | 32000138  |
| `LUXEMBOURG`                     | 32000139  |
| `MACAU`                          | 32000140  |
| `MACEDONIA_FYROM`                | 32000141  |
| `MADAGASCAR`                     | 32000142  |
| `MALAWI`                         | 32000143  |
| `MALAYSIA`                       | 32000144  |
| `MALDIVES`                       | 32000145  |
| `MALI`                           | 32000146  |
| `MALTA`                          | 32000147  |
| `MARSHALL_ISLANDS`               | 32000148  |
| `MARTINIQUE`                     | 32000149  |
| `MAURITANIA`                     | 32000150  |
| `MAURITIUS`                      | 32000151  |
| `MAYOTTE`                        | 32000152  |
| `MEXICO`                         | 32000153  |
| `MICRONESIA`                     | 32000154  |
| `MOLDOVA`                        | 32000155  |
| `MONACO`                         | 32000156  |
| `MONGOLIA`                       | 32000157  |
| `MONTENEGRO`                     | 32000158  |
| `MONTSERRAT`                     | 32000159  |
| `MOROCCO`                        | 32000160  |
| `MOZAMBIQUE`                     | 32000161  |
| `MYANMAR_BURMA`                  | 32000162  |
| `NAMIBIA`                        | 32000163  |
| `NAURU`                          | 32000164  |
| `NEPAL`                          | 32000165  |
| `NETHERLANDS`                    | 32000166  |
| `NEW_CALEDONIA`                  | 32000167  |
| `NEW_ZEALAND`                    | 32000168  |
| `NICARAGUA`                      | 32000169  |
| `NIGER`                          | 32000170  |
| `NIGERIA`                        | 32000171  |
| `NIUE`                           | 32000172  |
| `NORFOLK_ISLAND`                 | 32000173  |
| `NORTH_KOREA`                    | 32000174  |
| `NORTHERN_MARIANA_ISLANDS`       | 32000175  |
| `NORWAY`                         | 32000176  |
| `OMAN`                           | 32000177  |
| `PAKISTAN`                       | 32000178  |
| `PALAU`                          | 32000179  |
| `PALESTINE`                      | 32000180  |
| `PANAMA`                         | 32000181  |
| `PAPUA_NEW_GUINEA`               | 32000182  |
| `PARAGUAY`                       | 32000183  |
| `PERU`                           | 32000184  |
| `PHILIPPINES`                    | 32000185  |
| `PITCAIRN_ISLANDS`               | 32000186  |
| `POLAND`                         | 32000187  |
| `PORTUGAL`                       | 32000188  |
| `PUERTO_RICO`                    | 32000189  |
| `QATAR`                          | 32000190  |
| `REUNION`                        | 32000191  |
| `ROMANIA`                        | 32000192  |
| `RUSSIA`                         | 32000193  |
| `RWANDA`                         | 32000194  |
| `SAINT_BARTHELEMY`               | 32000195  |
| `SAINT_HELENA`                   | 32000196  |
| `SAINT_KITTS_AND_NEVIS`          | 32000197  |
| `SAINT_LUCIA`                    | 32000198  |
| `SAINT_MARTIN`                   | 32000199  |
| `SAINT_PIERRE_AND_MIQUELON`      | 32000200  |
| `SAMOA`                          | 32000201  |
| `SAN_MARINO`                     | 32000202  |
| `SAO_TOME_AND_PRINCIPE`          | 32000203  |
| `SAUDI_ARABIA`                   | 32000204  |
| `SENEGAL`                        | 32000205  |
| `SERBIA`                         | 32000206  |
| `SEYCHELLES`                     | 32000207  |
| `SIERRA_LEONE`                   | 32000208  |
| `SINGAPORE`                      | 32000209  |
| `SINT_MAARTEN`                   | 32000210  |
| `SLOVAKIA`                       | 32000211  |
| `SLOVENIA`                       | 32000212  |
| `SOLOMON_ISLANDS`                | 32000213  |
| `SOMALIA`                        | 32000214  |
| `SOUTH_AFRICA`                   | 32000215  |
| `SOUTH_KOREA`                    | 32000216  |
| `SOUTH_SUDAN`                    | 32000217  |
| `SPAIN`                          | 32000218  |
| `SRI_LANKA`                      | 32000219  |
| `ST_VINCENT_AND_GRENADINES`      | 32000220  |
| `SUDAN`                          | 32000221  |
| `SURINAME`                       | 32000222  |
| `SVALBARD_AND_JAN_MAYEN`         | 32000223  |
| `SWAZILAND`                      | 32000224  |
| `SWEDEN`                         | 32000225  |
| `SWITZERLAND`                    | 32000226  |
| `SYRIA`                          | 32000227  |
| `TAIWAN`                         | 32000228  |
| `TAJIKISTAN`                     | 32000229  |
| `TANZANIA`                       | 32000230  |
| `THAILAND`                       | 32000231  |
| `TIMOR_LESTE`                    | 32000232  |
| `TOGO`                           | 32000233  |
| `TOKELAU`                        | 32000234  |
| `TONGA`                          | 32000235  |
| `TRINIDAD_AND_TOBAGO`            | 32000236  |
| `TRISTAN_DA_CUNHA`               | 32000237  |
| `TUNISIA`                        | 32000238  |
| `TURKEY`                         | 32000239  |
| `TURKMENISTAN`                   | 32000240  |
| `TURKS_AND_CAICOS_ISLANDS`       | 32000241  |
| `TUVALU`                         | 32000242  |
| `US_OUTLYING_ISLANDS`            | 32000243  |
| `US_VIRGIN_ISLANDS`              | 32000244  |
| `UGANDA`                         | 32000245  |
| `UKRAINE`                        | 32000246  |
| `UNITED_ARAB_EMIRATES`           | 32000247  |
| `UNITED_KINGDOM`                 | 32000248  |
| `UNITED_STATES`                  | 32000249  |
| `URUGUAY`                        | 32000250  |
| `UZBEKISTAN`                     | 32000251  |
| `VANUATU`                        | 32000252  |
| `VATICAN_CITY`                   | 32000253  |
| `VENEZUELA`                      | 32000254  |
| `VIETNAM`                        | 32000255  |
| `WALLIS_AND_FUTUNA`              | 32000256  |
| `WESTERN_SAHARA`                 | 32000257  |
| `YEMEN`                          | 32000258  |
| `ZAMBIA`                         | 32000259  |
| `ZIMBABWE`                       | 32000260  |


