﻿WarFrequency Enum
=========
| **Name**       | **Value** |
|----------------|-----------|
| `NOT_SET`      | 0         |
| `ALWAYS`       | 1         |
| `TWICE_A_WEEK` | 2         |
| `ONCE_A_WEEK`  | 3         |
| `RARELY`       | 4         |
| `NEVER`        | 5         |


# WarFrequencyAlt Enum
| **Name**       | **Value** |
|----------------|-----------|
| `ANY`          | 0         |
| `NOT_SET`      | 0         |
| `ALWAYS`       | 1         |
| `NEVER`        | 2         |
| `TWICE_A_WEEK` | 3         |
| `ONCE_A_WEEK`  | 4         |
| `RARELY`       | 5         |
*(Alternative enum, used only for [`clanSearch`](../command/clanSearch.md), it's weird but that's how Clash works... Using the string-based versions is recommended, for future-proofing.)*


