﻿Getting Started
=========

## 1. Installation
Once you've been provided with your copy of Silver, installation is really a very simple process: just set the executable in a directory where you have permission to create files. Silver should only be run as administrator if executed from a directory locked to admin users only, which is generally unnecessary.

Linux users may have to take one extra step to execute Silver, simply marking their executable file as runnable:

```sh
cd DIRECTORY_SILVER_IS_IN
chmod +x silver
```

## 2. Configuration
Normally, Silver won't be shipped to you with a completely set-up configuration file (config.json), though help can be offered with this if need-be. Generally, it is advised to take a look at the [Configuration](config/config.md) documentation page for help on setting things up rather easily.

## 3. Launch
When you're ready to launch Silver, it's advised to create a new terminal/command prompt, change directory to your Silver folder if needed, then launch using the executable name.

By opening Silver through the command prompt on Windows, you avoid losing any logged data if the app itself closes, as things will be stored in the prompt. On Linux, using the terminal is necessary if you wish to view any output at all, which is generally a good idea.

**For Windows:**
```cmd
cd DIRECTORY_SILVER_IS_IN
Silver
```

**And on Linux:**
```sh
cd DIRECTORY_SILVER_IS_IN
./silver
```
*It is important to ensure that the `./` is added before the executable name on Linux, so that the shell can know to execute this file!*

## 4. Troubleshooting
Hopefully there haven't been any issue up to this point, though if you've encountered one please refer to the [Troubleshooting](troubleshooting.md) page or our [Skype group](https://join.skype.com/BZCFlDbztzKg).

## 5. Consuming the API
Once you have Silver up and running, it's time to start familiarizing yourself a bit with how the web-API works, for which the [Commands](command/commands.md) section should prove useful. It can be helpful to start simply playing around with commands in-browser to get a feel for the syntax and how things work.
```
http://127.0.0.1:80/api/userInfo?id=!9Y8YJQVY
```
*A sample command, demonstrating retrieval of profile information. Best viewed in Firefox, or formatted.*
